package cn.tm.ms.nerver;

import cn.tm.ms.nerver.idempotent.Idempotent;
import cn.tm.ms.nerver.idempotent.storage.impl.MemoryMapStorage;

/**
 * 幂等机制测试
 * @author lry
 */
public class IdempotentTest {

	public static void main(String[] args) {
		try {
			Idempotent it = new MemoryMapStorage();
			it.init(3000);
			while (true) {
				System.out.println(it.check("eeeee"));
				Thread.sleep(1000);
			}
		} catch (Throwable t) {
			t.printStackTrace();
		}
	}

}
