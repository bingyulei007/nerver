package cn.tm.ms.nerver.idempotent.storage;

/**
 * 幂等持久化
 * 
 * @author lry
 */
public interface IStorage {

	/**
	 * 检查是否包含KEY
	 * 
	 * @param key
	 * @return
	 */
	public boolean containsKey(String key);

	/**
	 * 根据KEY获取操作时间戳
	 * 
	 * @param key
	 * @return
	 */
	public long get(String key);

	/**
	 * 设置一个幂等操作
	 * 
	 * @param key
	 * @param time
	 */
	public void put(String key, long time);

	/**
	 * 清理
	 */
	public void cleanup();

}
