package cn.tm.ms.nerver.idempotent.entry;

import java.io.Serializable;

public class IdempotentData implements Serializable {

	private static final long serialVersionUID = 1L;

	/**
	 * 操作KEY
	 */
	private String key;
	/**
	 * 操作时间
	 */
	private long time;
	/**
	 * 操作次数
	 */
	private int times;

	public IdempotentData() {
	}

	public IdempotentData(String key, long time, int times) {
		this.key = key;
		this.time = time;
		this.times = times;
	}

	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}

	public long getTime() {
		return time;
	}

	public void setTime(long time) {
		this.time = time;
	}

	public int getTimes() {
		return times;
	}

	public void setTimes(int times) {
		this.times = times;
	}

}
