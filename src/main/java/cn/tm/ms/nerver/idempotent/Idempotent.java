package cn.tm.ms.nerver.idempotent;

/**
 * <b style="color:red;font-size:18px">幂等机制</b>
 * <p>
 * 使用场景:
 * <p>
 * 在业务开发中，我们常会面对防止重复请求的问题。当服务端对于请求的响应涉及数据的修改，或状态的变更时，可能会造成极大的危害。重复请求的后果在交易系统、
 * 售后维权，以及支付系统中尤其严重。前台操作的抖动，快速操作，网络通信或者后端响应慢，都会增加后端重复处理的概率。
 * 
 * @author lry
 */
public interface Idempotent {

	/**
	 * 初始化(时间窗为60000ms)
	 * 
	 * @throws Throwable
	 */
	public void init() throws Throwable;
	
	/**
	 * 初始化
	 * 
	 * @param timeWindow 时间窗
	 * @throws Throwable
	 */
	public void init(long timeWindow) throws Throwable;
	
	/**
	 * 幂等校验
	 * 
	 * @param key 校验值
	 * @return
	 */
	public boolean check(String key) throws Throwable;

	/**
	 * 销毁
	 */
	public void destroy();

}
