package cn.tm.ms.nerver.qos;

import org.apache.log4j.Logger;

import java.util.concurrent.atomic.AtomicInteger;

public class QosCounter {

    private static final Logger logger=Logger.getLogger(QosCounter.class);

    private final int maxflow;
    private AtomicInteger currentflow;
    private final String name;

    public QosCounter(String name) {
        this(-1, name);
    }

    public QosCounter(int maxflow, String name) {
        this.maxflow = maxflow;
        currentflow = new AtomicInteger(0);
        this.name = name;
    }

    public int getMaxflow() {
        return maxflow;
    }

    public int getCurrentflow() {
        return currentflow.get();
    }

    public void setCurrentflow(int currentflow) {
        this.currentflow = new AtomicInteger(currentflow);
    }

    public String getName() {
        return name;
    }

    public boolean incCounter() {
        for (;;) {
            int oldvalue = currentflow.get();
            int current = oldvalue + 1;
            if (current > maxflow) {
                return false;
            }
            if (currentflow.compareAndSet(oldvalue, current)) {
                logger.info(Thread.currentThread() + "加流量"+current);
                return true;
            }
        }
    }

    public boolean decCounter() {
        for (;;) {
            int oldvalue = currentflow.get();
            int current = oldvalue - 1;
            if (current < 0) {
                return false;
            }
            if (currentflow.compareAndSet(oldvalue, current)) {
                logger.info(Thread.currentThread() + "减流量"+current);
                return true;
            }
        }
    }
}